<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Repositories\AuthorRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Returns the list of authors
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $authors = Author::all();
        return $this->successResponse($authors);
    }

    /**
     * Create a new author
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, AuthorRepository $authorRepository)
    {
        $this->validate($request, Author::rules());
        if ($authorRepository->checkIfExist($request)) {
            return $this->errorResponse('Sorry, this author is already registered!', 409);
        }
        $author = Author::create($request->all());
        return $this->successResponse($author, Response::HTTP_CREATED);
    }



    /**
     * Obtain and Show one author
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($author)
    {
        $author = Author::findOrFail($author);
        return $this->successResponse($author);
    }

    /**
     * Update an existing author
     * @param Request $request
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $author)
    {
        $this->validate($request, Author::rules_update());
        $author = Author::findOrFail($author);
        $author->fill($request->all());
        if($author->isDirty()){
            $author->save();
            return $this->successResponse($author);
        }
        return $this->errorResponse('You must enter at least a different attribute', Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Remove an existing author
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($author)
    {
        $author = Author::findOrFail($author);
        $author->delete();
        return $this->successResponse($author);
    }

}
