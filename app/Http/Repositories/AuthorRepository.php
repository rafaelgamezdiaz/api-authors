<?php


namespace App\Http\Repositories;

use App\Author;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AuthorRepository
{
    use ApiResponser;

    public $rules = [
        'name'      => 'required|max:255',
        'gender'    => 'required|max:6|in:male,female',
        'country'   => 'required|max:255'
    ];

    /**
     * Check if the Author is already registered
     * @param $request
     * @return bool
     */
    public function checkIfExist($request)
    {
        $author = Author::where('name', $request->name)
                        ->where('gender', $request->gender)
                        ->where('country', $request->country)
                        ->get();
        return count($author) > 0;
    }


}
