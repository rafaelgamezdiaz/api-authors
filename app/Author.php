<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'name',
        'gender',
        'country'
    ];

    public static function rules()
    {
        return [
            'name'      => 'required|max:255',
            'gender'    => 'required|max:6|in:male,female',
            'country'   => 'required|max:255'
        ];
    }

    public static function rules_update()
    {
        return [
            'name'      => 'max:255',
            'gender'    => 'max:6|in:male,female',
            'country'   => 'max:255'
        ];
    }

}
